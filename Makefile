build:
	mvn clean install
run:
	mvn spring-boot:run
docker:
	mvn spring-boot:build-image -Dspring-boot.build-image.imageName=infinispan-test
docker-run:
	docker run -p 9090:9090 -t infinispan-test
init:
	curl -d '{ "code" : "code", "name" : "name", "bic" : "bic" }' -H 'Content-Type: application/json' localhost:9090
load:
	k6 run k6/script.js