package com.progressoft.infinispan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Participant implements Serializable {
    private int id;
    private String code;
    private String name;
    private String bic;
}
