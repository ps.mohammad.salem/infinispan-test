package com.progressoft.infinispan;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ParticipantRequest {
    @NotBlank
    private String code;
    @NotBlank
    private String name;
    @NotBlank
    private String bic;
}
