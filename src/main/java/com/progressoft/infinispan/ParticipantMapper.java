package com.progressoft.infinispan;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ParticipantMapper {
    Participant toModel(ParticipantRequest request);

    Participant toModel(ParticipantEntity source);

    ParticipantEntity toEntity(Participant source);
}