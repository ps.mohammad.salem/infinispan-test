package com.progressoft.infinispan;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.spring.starter.embedded.InfinispanCacheConfigurer;
import org.infinispan.spring.starter.embedded.InfinispanGlobalConfigurationCustomizer;
import org.infinispan.spring.starter.embedded.InfinispanGlobalConfigurer;
import org.infinispan.transaction.LockingMode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public InfinispanCacheConfigurer cacheConfigurer() {
        return manager -> manager.defineConfiguration("customers", new ConfigurationBuilder()
                .clustering().stateTransfer().awaitInitialTransfer(false)
                .clustering().cacheMode(CacheMode.REPL_ASYNC)
                .transaction().autoCommit(true).lockingMode(LockingMode.PESSIMISTIC)
                .statistics().enable()
                .build());
    }

    @Bean
    public InfinispanGlobalConfigurer globalConfigurer() {
        return () -> new GlobalConfigurationBuilder()
                .cacheContainer().statistics(true)
                .metrics().gauges(true).histograms(true)
                .jmx().enable()
                .transport().defaultTransport()
                .build();
    }

    @Bean
    public InfinispanGlobalConfigurationCustomizer globalConfigurationCustomizer() {
        return builder -> builder.cacheManagerName("customers")
                .serialization().whiteList().addClasses(Participant.class);
    }

    @Bean
    public Cache<Integer, Participant> customersCache(EmbeddedCacheManager cacheManager) {
        return cacheManager.getCache("customers");
    }
}
