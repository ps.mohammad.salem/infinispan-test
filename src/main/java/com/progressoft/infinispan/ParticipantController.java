package com.progressoft.infinispan;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
public class ParticipantController {
    private final ParticipantService service;

    @GetMapping("{id}")
    public Participant get(@PathVariable("id") int id) {
        return service.get(id);
    }

    @PostMapping
    public Participant post(@Valid @RequestBody ParticipantRequest request) {
        return service.add(request);
    }

    @PutMapping("{id}")
    public Participant put(@PathVariable("id") int id, @Valid @RequestBody ParticipantRequest request) {
        return service.update(id, request);
    }
}
