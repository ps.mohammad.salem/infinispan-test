package com.progressoft.infinispan;

import lombok.AllArgsConstructor;
import org.infinispan.Cache;
import org.springframework.stereotype.Service;

import static java.util.Optional.ofNullable;

@Service
@AllArgsConstructor
public class ParticipantService {
    private final ParticipantMapper mapper;
    private final JpaParticipantEntityRepository repository;
    private final Cache<Integer, Participant> customersCache;

    public Participant get(int id) {
        return ofNullable(customersCache.get(id)).orElseGet(() -> mapper.toModel(repository.getOne(id)));
    }

    public Participant add(ParticipantRequest request) {
        Participant participant = save(mapper.toModel(request));
        customersCache.put(participant.getId(), participant);
        return participant;
    }

    public Participant update(int id, ParticipantRequest request) {
        Participant participant = save(update(request, customersCache.get(id)));
        customersCache.put(id, participant);
        return participant;
    }

    private Participant update(ParticipantRequest request, Participant participant) {
        participant.setCode(request.getCode());
        participant.setName(request.getName());
        participant.setBic(request.getBic());
        return participant;
    }

    private Participant save(Participant participant) {
        return mapper.toModel(repository.save(mapper.toEntity(participant)));
    }
}
